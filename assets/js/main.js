$(window).scroll(function () {
    var positionY = $(window).scrollTop();
    console.log(positionY);
    console.log(Math.floor(positionY / 10));
    if (positionY < 100) {
        $('#navbar').css("background-color", "rgba(57, 62, 70,." + Math.floor(positionY / 10) + 1 + ")");
    }
    if (positionY > 400) {
        $('#arrow').hide();
        console.log('arrow hide');
        $('#navbar').css("background-color", "rgba(57, 62, 70,1");
    }

    $(".header-section .navbar-section").css('padding-top', '0');
    $(".header-section .navbar-section").css('padding-bottom', '0');
});

$('.message-text').css('opacity', '0');


var setMessageTextOpacity = function () {
    $('.message-text').css('opacity', '1');
}

setTimeout(setMessageTextOpacity, 2500);

var positionY = $(window).scrollTop();
if (positionY > 100) {
    $('#navbar').css("background-color", "rgba(57, 62, 70,." + Math.floor(positionY / 10) + ")");
    if (positionY > 400) {
        $('#arrow').css("display", "none");
    }
}

if ($('blog-page')) {


    //#region  data
    var data = [{
            "data": "06/01/2018",
            "postedBy": "Mate Daniel",
            "title": "La Bandera"
        },
        {
            "data": "01/21/2017",
            "postedBy": "Mate Daniel",
            "title": "D.A.R.Y.L."
        },
        {
            "data": "07/11/2018",
            "postedBy": "Mate Daniel",
            "title": "The Hire: Chosen"
        },
        {
            "data": "02/02/2018",
            "postedBy": "Mate Daniel",
            "title": "Lan Yu"
        },
        {
            "data": "02/13/2017",
            "postedBy": "Mate Daniel",
            "title": "Black Dynamite"
        },
        {
            "data": "11/13/2017",
            "postedBy": "Mate Daniel",
            "title": "Pray the Devil Back to Hell"
        },
        {
            "data": "05/26/2017",
            "postedBy": "Mate Daniel",
            "title": "Ju-on: White Ghost"
        },
        {
            "data": "06/25/2017",
            "postedBy": "Mate Daniel",
            "title": "Untamed Heart"
        },
        {
            "data": "02/28/2017",
            "postedBy": "Mate Daniel",
            "title": "Bitch, The (La chienne)"
        },
        {
            "data": "03/31/2018",
            "postedBy": "Mate Daniel",
            "title": "Jerusalem Countdown"
        }
    ];
    //#endregion

    console.log(data);

    data.sort(function (a, b) {
        var c = new Date(a.data);
        var d = new Date(b.data);
        return d - c;
    });
}

data.forEach(element => {
    var html = `<div class="post-item">
                <div class="card mb-4">
                    <div class="post-image"></div>
                    <div class="card-body">
                      <h2 class="card-title">${element.title}</h2>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                      <a href="#" class="btn btn-primary">Read More →</a>
                    </div>
                    <div class="card-footer text-muted">
                      Posted on ${element.data} by
                      <a href="#">${element.postedBy}</a>
                    </div>
                </div>
            </div>`;
    $('.post-list').append(html);
});

$('.recent-posts').children().remove();
for (let i = 0; i < 5 && i < data.length; i++) {
    let html = `<a href="#" class="row post-item"><h6 class="col-9">${data[i].title}</h6><span class="col-3 recent-item-date">${data[i].data}</span></a>`
    $('.recent-posts').append(html);
}

var search = $('#searchByTitle');

search.change(() => {
console.log(search.val());
$('.post-list').children().remove();


data.forEach(element => {
    if (element.title.includes(search.val()) || search.val() == '') {
        var html = `<div class="post-item">
                 <div class="card mb-4">
                     <div class="post-image"></div>
                     <div class="card-body">
                       <h2 class="card-title">${element.title}</h2>
                       <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                       <a href="#" class="btn btn-primary">Read More →</a>
                     </div>
                     <div class="card-footer text-muted">
                       Posted on ${element.data} by
                       <a href="#">${element.postedBy}</a>
                     </div>
                 </div>
             </div>`;
        $('.post-list').append(html);
    }
});
});