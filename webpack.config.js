
var Encore = require('@symfony/webpack-encore');
const { VueLoaderPlugin } = require('vue-loader');



Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')

    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    // shared entry used in all files
    .createSharedEntry("vendor", './assets/js/common.js')
    .cleanupOutputBeforeBuild()
    // .enableBuildNotifications()
    // .enableSourceMaps(!Encore.isProduction())
    // .enableVersioning(Encore.isProduction())
    // .enableSassLoader();
   
;

var config = Encore.getWebpackConfig();


var ResolvePath = function(filePath){
    let path = require('path');
    return path.resolve(__dirname, filePath);
}



module.exports = config;




